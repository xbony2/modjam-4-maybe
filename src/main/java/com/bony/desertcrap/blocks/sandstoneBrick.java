package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class sandstoneBrick extends Block{

	@SideOnly(Side.CLIENT)
	private IIcon topBottom;
	
	public sandstoneBrick(Material material) {
		super(material);
		
		this.setStepSound(soundTypePiston);
		this.setHardness(0.8F);
		this.setBlockName("sandstoneBrick");
		this.setCreativeTab(desertcrap.desertcrap);
	}

	public IIcon getIcon(int side, int meta){
		switch(side){
		
		case 1: return topBottom;
		case 2: return blockIcon;
		case 3: return blockIcon;
		case 4: return blockIcon;
		case 5: return blockIcon;
		default: return topBottom;
		}
	}
	
	public void registerBlockIcons(IIconRegister register){
		blockIcon = register.registerIcon("bonydesert" + ":" + "sandstone_brick");
		topBottom = register.registerIcon("minecraft" + ":" + "sandstone_top");
	}
}

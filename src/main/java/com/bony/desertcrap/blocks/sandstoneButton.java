package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockButton;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;

public class sandstoneButton extends BlockButton{

	public sandstoneButton() {
		super(false);
		
		this.setHardness(0.5F);
		this.setStepSound(soundTypePiston);
		this.setBlockName("sandstoneButton");
		this.setCreativeTab(desertcrap.desertcrap);
		
	}

	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta){
        return Blocks.sandstone.getBlockTextureFromSide(1);
    }
}

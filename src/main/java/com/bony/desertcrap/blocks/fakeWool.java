package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

public class fakeWool extends Block{

	public fakeWool(Material material) {
		super(material);
		this.setHardness(0.8F);
		this.setStepSound(soundTypeCloth);
		this.setBlockName("fakeWool");
		this.setCreativeTab(desertcrap.desertcrap);
		
	}

	public void registerBlockIcons(IIconRegister register){
		blockIcon = register.registerIcon("bonydesert" + ":" + "fakewool");
	}
}

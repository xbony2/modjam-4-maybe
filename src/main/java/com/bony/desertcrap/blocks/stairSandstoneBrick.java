package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;

public class stairSandstoneBrick extends BlockStairs{

	public stairSandstoneBrick(Block block, int par2) {
		super(desertcrap.SandstoneBrick, 0);
		this.setCreativeTab(desertcrap.desertcrap);
		this.setBlockName("stairSandstoneBrick");
	}

}
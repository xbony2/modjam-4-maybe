package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class cactusWood extends Block{
	public cactusWood(Material material) {
		super(material.wood);
		this.setHardness(2.0F);
		this.setResistance(5.0F);
		this.setStepSound(soundTypeWood);
		this.setBlockName("woodCactus");
		
		this.setCreativeTab(desertcrap.desertcrap);
		}

	public void registerBlockIcons(IIconRegister register){
		blockIcon = register.registerIcon("bonydesert" + ":" + "cactus_wood");
	}
}

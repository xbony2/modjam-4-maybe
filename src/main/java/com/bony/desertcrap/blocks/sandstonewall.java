package com.bony.desertcrap.blocks;

import java.util.List;

import com.bony.desertcrap.desertcrap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.BlockWall;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class sandstonewall extends BlockWall{

	public sandstonewall(Block block) {
		super(block);
		this.setCreativeTab(desertcrap.desertcrap);
		this.setBlockName("wallSandstone");
	}

	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta){
        return Blocks.sandstone.getBlockTextureFromSide(side);
    }
	 @SideOnly(Side.CLIENT)
	 public void getSubBlocks(Item par1, CreativeTabs par2, List par3){
		 par3.add(new ItemStack(par1, 1, 0));
	 }
}

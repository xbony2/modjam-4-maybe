package com.bony.desertcrap.blocks;

import com.bony.desertcrap.desertcrap;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;

public class stairCactusWood extends BlockStairs{

	public stairCactusWood(Block block, int par2) {
		super(desertcrap.CactusWood, 0);
		this.setCreativeTab(desertcrap.desertcrap);
		this.setBlockName("stairCactusWood");
	}

}

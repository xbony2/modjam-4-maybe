package com.bony.desertcrap.blocks;

import java.util.Random;

import com.bony.desertcrap.desertcrap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class slabbricksandstone extends BlockSlab{

	public slabbricksandstone(boolean bool, Material material) {
		super(bool, material);
		
	}

	public String func_150002_b(int p_150002_1_){
		return super.getUnlocalizedName();
    }

	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta){
        return desertcrap.SandstoneBrick.getIcon(side, meta & 7);
    }
	
	public Item getItemDropped(int par1, Random rand, int par2){
        return Item.getItemFromBlock(desertcrap.SlabSandstoneBrick);
    }
	
	protected ItemStack createStackedBlock(int thing){
        return new ItemStack(Item.getItemFromBlock(desertcrap.SlabSandstoneBrick), 2, thing & 7);
    }

}


package com.bony.desertcrap.items;

import com.bony.desertcrap.desertcrap;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemFood;

public class cactusFruit extends ItemFood{

	public cactusFruit(int hungerGoten) {
		super(hungerGoten, false);
		this.setMaxStackSize(64);
		this.setUnlocalizedName("cactusFruit");
		this.setCreativeTab(desertcrap.desertcrap);
	}

	public void registerIcons(IIconRegister register){
		itemIcon = register.registerIcon("bonydesert" + ":" + "cactus_fruit");
	}
}

package com.bony.desertcrap.items;

import com.bony.desertcrap.desertcrap;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSword;

public class sandstoneHoe extends ItemHoe{

	public sandstoneHoe(ToolMaterial toolmaterial) {
		super(toolmaterial);
		this.setUnlocalizedName("sandstoneHoe");
		this.setCreativeTab(desertcrap.desertcrap);
		this.setMaxStackSize(1);
	}
	public void registerIcons(IIconRegister register){
		itemIcon = register.registerIcon("bonydesert" + ":" + "sandstone_hoe");
	}
}
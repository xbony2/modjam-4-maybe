package com.bony.desertcrap.items;

import com.bony.desertcrap.desertcrap;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemSword;

public class sandstoneSword extends ItemSword{

	public sandstoneSword(ToolMaterial toolmaterial) {
		super(toolmaterial);
		this.setCreativeTab(desertcrap.desertcrap);
		this.setMaxStackSize(1);
		this.setUnlocalizedName("sandstoneSword");
	}
	public void registerIcons(IIconRegister register){
		itemIcon = register.registerIcon("bonydesert" + ":" + "sandstone_sword");
	}
}

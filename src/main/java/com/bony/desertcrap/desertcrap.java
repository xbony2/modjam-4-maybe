package com.bony.desertcrap;

import com.bony.desertcrap.blocks.cactusWood;
import com.bony.desertcrap.crafting.shapedrecipes;
import com.bony.desertcrap.crafting.shapelessrecipes;
import com.bony.desertcrap.crafting.smeltingrecipes;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = "bonydesert", version = "1.0.0a")
public class desertcrap{
    /**I solemnly swear I am up to no good!*/
	public static Block CactusWood;
	public static Block SandstoneBrick;
	public static Block fakeWool;
	public static Block SlabCactusWood;
	public static Block DoubleSlabCactusWood;
	public static Block buttonSandstone;
	public static Block stairCactusWood;
	public static Block WallSandstone;
	public static Block SlabSandstoneBrick;
	public static Block DoubleSlabSandstoneBrick;
	public static Block stairSandstoneBrick;
	
	public static Item CactusFruit;
	public static Item SandstoneSword;
	public static Item SandstonePick;
	public static Item SandstoneAxe;
	public static Item SandstoneSpade;
	public static Item SandstoneHoe;
	
	public static CreativeTabs desertcrap;
	
	
	@EventHandler
    public void preinit(FMLInitializationEvent event){
		
		desertcrap = new CreativeTabs("bonydesertcraft"){
			@SideOnly(Side.CLIENT)
			public Item getTabIconItem() {
				return Item.getItemFromBlock(CactusWood); //The cacus wood
			}
		};
		
		CactusFruit = new com.bony.desertcrap.items.cactusFruit(4);
		SandstoneSword = new com.bony.desertcrap.items.sandstoneSword(ToolMaterial.STONE);
		SandstonePick = new com.bony.desertcrap.items.sandstonePick(ToolMaterial.STONE);
		SandstoneAxe = new com.bony.desertcrap.items.sandstoneAxe(ToolMaterial.STONE);
		SandstoneSpade = new com.bony.desertcrap.items.sandstoneSpade(ToolMaterial.STONE);
		SandstoneHoe = new com.bony.desertcrap.items.sandstoneHoe(ToolMaterial.STONE);
		
		GameRegistry.registerItem(CactusFruit, "Cactus Fruit");
		GameRegistry.registerItem(SandstoneSword, "Sandstone Sword");
		GameRegistry.registerItem(SandstonePick, "Sandstone Pick");
		GameRegistry.registerItem(SandstoneAxe, "Sandstone Axe");
		GameRegistry.registerItem(SandstoneSpade, "Sandstone Spade");
		GameRegistry.registerItem(SandstoneHoe, "Sandstone Hoe");
		
		CactusWood = new com.bony.desertcrap.blocks.cactusWood(Material.wood);
		SandstoneBrick = new com.bony.desertcrap.blocks.sandstoneBrick(Material.rock);
		fakeWool = new com.bony.desertcrap.blocks.fakeWool(Material.cloth);
		SlabCactusWood = new com.bony.desertcrap.blocks.cactusWoodSlab(false, Material.wood).setCreativeTab(desertcrap).setHardness(2.0F).setResistance(5.0F).setBlockName("slabCactusWood");
		DoubleSlabCactusWood = new com.bony.desertcrap.blocks.cactusWoodSlab(true, Material.wood).setHardness(2.0F).setResistance(5.0F).setBlockName("doubleSlabCactusWood");
		buttonSandstone = new com.bony.desertcrap.blocks.sandstoneButton();
		stairCactusWood = new com.bony.desertcrap.blocks.stairCactusWood(CactusWood, 0);
		WallSandstone = new com.bony.desertcrap.blocks.sandstonewall(Blocks.sandstone);
		SlabSandstoneBrick = new com.bony.desertcrap.blocks.slabbricksandstone(false, Material.rock).setCreativeTab(desertcrap).setHardness(0.8F).setBlockName("slabBrickSandstone");
		DoubleSlabSandstoneBrick = new com.bony.desertcrap.blocks.slabbricksandstone(true, Material.rock).setHardness(0.8F).setBlockName("slabsandstoneBrick");
		stairSandstoneBrick = new com.bony.desertcrap.blocks.stairSandstoneBrick(SandstoneBrick, 0);
		
		GameRegistry.registerBlock(CactusWood, "Cactus Wood");
		GameRegistry.registerBlock(SandstoneBrick, "Sandstone Brick");
		GameRegistry.registerBlock(fakeWool, "Fake Wool");
		GameRegistry.registerBlock(SlabCactusWood, "Slab Cactus Wood");
		GameRegistry.registerBlock(DoubleSlabCactusWood, "DoubleSlab Cactus Wood");
		GameRegistry.registerBlock(buttonSandstone, "Button Sandstone");
		GameRegistry.registerBlock(stairCactusWood, "Stair Cactus Wood");
		GameRegistry.registerBlock(WallSandstone, "Sandstone Wall");
		GameRegistry.registerBlock(SlabSandstoneBrick, "Sandstone Brick Slab");
		GameRegistry.registerBlock(DoubleSlabSandstoneBrick, "Double Sandstone Brick Slab icecream");
		GameRegistry.registerBlock(stairSandstoneBrick, "Stair Sandstone Brick");
		
		OreDictionary.registerOre("plankWood", CactusWood);
		
		//Registers Shaped recipes
		shapedrecipes.registerShapedRecipes();
		
		//Registers Shapeless recipes
		shapelessrecipes.registerShaplessRecipes();
		
		//Registers Smelting recipes
		smeltingrecipes.registerSmelting();
			
    }
	
    @EventHandler
    public void init(FMLInitializationEvent event){
		
    }
    
    @EventHandler
    public void postinit(FMLInitializationEvent event){
		
    }
}

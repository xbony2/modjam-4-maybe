package com.bony.desertcrap.crafting;

import com.bony.desertcrap.desertcrap;
import com.bony.desertcrap.blocks.sandstoneButton;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class shapelessrecipes {

	public static void registerShaplessRecipes(){
		GameRegistry.addShapelessRecipe(new ItemStack(desertcrap.CactusWood, 3), Blocks.cactus);
		
		GameRegistry.addShapelessRecipe(new ItemStack(desertcrap.buttonSandstone), Blocks.sandstone);
		
		GameRegistry.addShapelessRecipe(new ItemStack(Items.clay_ball, 3), Blocks.sand, Blocks.gravel, Blocks.dirt);
	}
}

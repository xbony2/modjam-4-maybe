package com.bony.desertcrap.crafting;

import com.bony.desertcrap.desertcrap;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class shapedrecipes {

	public static void registerShapedRecipes(){
		GameRegistry.addRecipe(new ItemStack(desertcrap.CactusFruit, 8), new Object[]{
			"cc",
			"cc", 'c', Blocks.cactus});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneSword), new Object[]{
		"s",
		"s",
		"r", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstonePick), new Object[]{
		"sss",
		" r ",
		" r ", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneAxe), new Object[]{
		"ss ",
		"sr ",
		" r ", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneAxe), new Object[]{
		" ss",
		" rs",
		" r ", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneSpade), new Object[]{
		"s",
		"r",
		"r", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneHoe), new Object[]{
		"ss ",
		" r ",
		" r ", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneHoe), new Object[]{
		" ss",
		" r ",
		" r ", 's', Blocks.sandstone, 'r', Items.stick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneBrick, 4), new Object[]{
		"rs",
		"sr", 'r', Blocks.sandstone, 's', Blocks.sand});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SandstoneBrick, 4), new Object[]{
		"sr",
		"rs", 'r', Blocks.sandstone, 's', Blocks.sand});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.fakeWool, 4), new Object[]{
		"lll",
		"lsl",
		"lll", 's', Items.leather, 'l', Blocks.sand});
	
	GameRegistry.addRecipe(new ItemStack(Items.bed), new Object[]{
		"www",
		"ppp", 'w', desertcrap.fakeWool, 'p', desertcrap.CactusWood});
	
	GameRegistry.addRecipe(new ItemStack(Items.painting), new Object[]{
		"sss",
		"sws",
		"sss", 's', Items.stick, 'w', desertcrap.fakeWool });
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SlabCactusWood, 6), new Object[]{
		"www", 'w', desertcrap.CactusWood});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.stairCactusWood, 4), new Object[]{
		"  c",
		" cc",
		"ccc", 'c', desertcrap.CactusWood});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.WallSandstone, 6), new Object[]{
		"sss",
		"sss", 's', Blocks.sandstone});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.SlabSandstoneBrick, 6), new Object[]{
		"bbb", 'b', desertcrap.SandstoneBrick});
	
	GameRegistry.addRecipe(new ItemStack(desertcrap.stairSandstoneBrick, 4), new Object[]{
		"  s",
		" ss",
		"sss", 's', desertcrap.SandstoneBrick});
	
	}
}
